from participate import Participate

class Group:
    """
    interface class for Group
    """
    def __init__(self, group):

        # private fields
        self._num = None
        self._name =None
        self._participate = []
        self._group = group
        # self._version = group.get('_version')
        # self._elastic_id = group.get('_elastic_id')
        
        # parse params
        try:
            self.num = group['num']
            self.name = group['name']
            self.participate = group.get('participate')
        except KeyError as key_err:
            raise KeyError('field "%s" not set' % key_err)

    #
    # field: name
    #

    @property
    def name(self):
        """
        getter for field name
        Returns:
            name: if set
            ' ': if not set
        """
        return self._name if self._name else ''

    @name.setter
    def name(self, value):
        """
        setter for field name
        Args:
            value: str group name
        Raises:
            TypeError: invalid type of
                       param name
        """
        try:
            self._name = str(value)
        except TypeError as type_err:
            raise TypeError(
                'invalid value: %s for param "name"'
                % type_err
            )

    #
    # field: num
    #

    @property
    def num(self):
        """
        getter for field num
        Returns:
            group number: if set
            -1: if not set
        """
        return self._num if self._num else -1

    @num.setter
    def num(self, value):
        """
        setter for field num
        Args:
            value: str or int, group number
        Raises:
            TypeError: invalid type of
                       param num
            ValueError: invalid value of param
        """
        try:
            val = int(value)
            if val < 0 or val > 10000:
                raise ValueError(
                    'invalid value for field: "num": %d'
                    % val
                )
            self._num = val
        except TypeError as type_err:
            raise TypeError(
                'invalid value: %s for param "num"'
                % type_err
            )

    #
    # field: participate
    #

    @property
    def participate(self):
        """
        getter for field participate
        Returns:
            list of participates: if set
            [ ]: if not set
        """
        return self._participate if self._participate else []

    @participate.setter
    def participate(self, value):
        """
        setter for field participate
        Args:
            value: dict, group participates
        Raises:
            TypeError :invalid type of
                       param participate
        """
        if not value:
            self._participate = []

        if not isinstance(value, dict):
            raise TypeError('invalid type for field participate (excepted: dict)')

        self._participate = []        

        for key in value:
            self._participate.append(Participate(participate=value[key]))

    def __str__(self):
        """
        pretty print object 
        Return:
            formated string with all fields
        """
        return (
            'name: {self.name}, number: {self.num} participate: {self.participate}'
            .format(self=self)
        )


def create_group(num, name):
    """
    create new group
    Args:
        num: str or int, group number 
        name: str, group name
        leader: str, group leader
        participate: dict, group participate
    Returns:
        Group object: if created
    """
    group = {
        'num': num,
        'name': name,
        'leader': None,
        # NOTE THIS FIELD NEED TO UPDATE DOCUMENT
        'participate': {}
    }
    return Group(group=group)


