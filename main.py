from elastic_cluster import ElasticCluster
from elastic_storage import ElasticStorage
from group import create_group
from participate import create_participate
from time import sleep

HOSTS = [{"host": '127.0.0.1', "port": 9200},]
INDEX = 'storm'
TYPE = 'boat'

if __name__ == '__main__':
    el = ElasticCluster(index=INDEX, type=TYPE)
    es = ElasticStorage(index=INDEX, type=TYPE)

    try:
        es.delete_index()
        print('delete mapping')

    except Exception as e:
        # ignode
        print('pass to delete mapping')
        pass

    # create new group
    egle = create_group(num=123, name='egle')

    # create few participate
    mark = create_participate(name='mark', age=23, country='Japan')
    peter = create_participate(name='peter', age=90, country='Spain')

    el.create_mapping()
    if el.add_group(group=egle):
        print('add new group')
    sleep(1)

    if el.add_participate(group_num=123, participate=mark):
        print('add new participate: mark')

    if el.add_participate(group_num=123, participate=peter):
        print('participate peter added')

    group = el.get_by_group_num(group_num=123)
    print('Group info: %s' % group)
