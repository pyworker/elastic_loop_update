
class Participate:
    """
    interface class for Participate 
    """
    def __init__(self, participate):
        
        # private fields
        self._name = None
        self._age = None
        self._country = None
        self._participate = participate

        # parse args
        try:
            self.name = participate['name']
            self.age = participate['age']
            self.country = participate['country']
        except KeyError as key_err:
            raise KeyError('field "%s" not set' % key_err)

    #
    # field: name
    #

    @property
    def name(self):
        """
        getter for field name
        Returns:
            name: if set
            ' ': if not set
        """
        return self._name if self._name else ''

    @name.setter
    def name(self, value):
        """
        setter for field
        Args:
            value: str
        Raises:
            TypeError invalid type of param
        """
        try:
            self._name = str(value)
        except TypeError as type_err:
            raise TypeError(
                'invalid value: %s for param "name"'
                % type_err
            )

    #
    # field: country
    #

    @property
    def country(self):
        """
        getter for field country
        Returns:
            country: if set
            ' ': if not set
        """
        return self._country if self._country else ''

    @country.setter
    def country(self, value):
        """
        setter for field
        Args:
            value: str
        Raises:
            TypeError invalid type of param
        """
        try:
            self._country = str(value)
        except TypeError as type_err:
            raise TypeError(
                'invalid value: %s for param "country"'
                % type_err
            )

    #
    # field: age
    #

    @property
    def age(self):
        """
        getter for field age
        Returns:
            age: if set
            -1 : if not set
        """
        return self._age if self._age else -1

    @age.setter
    def age(self, value):
        """
        setter for field age
        Args:
            value: str or int, participate age
        Raises:
            TypeError invalid type of param age
        """
        try:
            val = int(value)
            if val < 0 or val > 120:
                raise ValueError(
                    'invalid value for field: "age": %d'
                    % val
                )
            self._age = val
        except TypeError as type_err:
            raise TypeError(
                'invalid value: %s for param "age"'
                % type_err
            )


    def __str__(self):
        """
        pretty print object 
        Return:
            formated string with all fields
        """
        return (
            'name: {self.name} age: {self.age} country: {self.country}'
            .format(self=self)
        )

def create_participate(name, age, country):
    """
    create new participate
    Args:
        name: str, participate name 
        age: int, participate age 
        country: str, participate country
    Raises:
        TypeError: invalid args
    Returns:
        Participate object: if created
    """
    try:
        participate = {
            'name': str(name),
            'age': int(age),
            'country': str(country)
        }

    except TypeError as type_err:
        raise TypeError('fialed to create participate: invalid args %s' % type_err)

    return Participate(participate=participate)
