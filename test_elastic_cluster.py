import unittest
from time import sleep

from elastic_cluster import ElasticCluster
from elastic_storage import ElasticStorage
from participate import create_participate
from group import create_group


INDEX = 'orange_book'
TYPE = 'lollipop'


class TestElasticUser(unittest.TestCase):
    """docstring for TestElasticUser"""
    def test_create_mapping(self):
        """
        currect work create mapping
        except: None
        """
        ec = ElasticCluster(index=INDEX, type=TYPE)
        es = ElasticStorage(index=INDEX, type=TYPE)

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass

        ec.create_mapping()

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass


    def test_update_item(self):
        """
        currect work update item
        """

        ec = ElasticCluster(index=INDEX, type=TYPE)
        es = ElasticStorage(index=INDEX, type=TYPE)

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass

        ec.create_mapping()
        egle = create_group(num=123, name='egle')
        ec.add_group(group=egle)
        print('new group added')
        sleep(3)


        mark = create_participate(name='mark', age=23, country='Japan')
        ec.add_participate(group_num=123, participate=mark)

        group = ec.get_by_group_num(group_num=123)
        print('Group info: %s' % group)

        try:
            # prepare index before test
            es.delete_index()
            print('delete index - ok')
        except Exception:
            # ignore
            pass


if __name__ == '__main__':
    unittest.main()
