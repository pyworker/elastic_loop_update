from elasticsearch import Elasticsearch, TransportError, RequestError
from group import Group, Participate

from elastic_storage import ElasticStorage

class ElasticCluster:

    def __init__(self, index, type):
        self._type = type
        self._index = index
        self._es = ElasticStorage(index=index, type=type)

    def create_mapping(self):
        """
        create mapping for index
        """
        mapping = {
            'mappings': {
                self._type: {
                    'properties': {
                        'name': {
                            'type': 'text',
                            'index': 'true'
                        },
                        'num': {
                            'type': 'integer',
                            'index': 'true'
                        },
                        'leader': {
                            'type': 'text',
                            'index': 'true'
                        },
                        'participate': {
                            'properties': {}
                        }}}}}

        self._es.create_index(mapping=mapping)

    def add_group(self, group):
        """
        add new group to elastic index
        Args:
            item: dict, item for add
        Raises:
            TypeError: group has invalid type
        Returns: 
            True: if item added to the elastic index
            False: if item NOT added to the elastic index
        """
        if not isinstance(group, Group):
            raise TypeError('invalid type of param: except Group')
        return self._es.add(document=group._group)

    def add_participate(self, group_num, participate):
        """
        add new participate to existing group
        Args:
            group_num: str, group number
            participate: Participate object
        Raises:
            Exception: invalid object participate
                       failed to get group
                       group not exist
        Returns:
            None: if not find result
        """

        try:
            responce = self.get_by_group_num(group_num=group_num)
            group = Group(responce.source)

            if not group:
                raise Exception('group with number: %s not exist' % group_num)
        except Exception as ex:
            raise Exception('failed to get group')
        if not isinstance(participate, Participate):
            raise Exception('invalid object participate')

        return self.update_item(item=responce, participate=participate._participate)

    def get_by_group_num(self, group_num):
        """
        Args:
            group_num: str or int, group number
        Raises:
        Returns: 
            Group object: if group found
            None: if group not found
        """
        search_query = {
            "version": True,
            "query": {
                "match": {"num": int(group_num)}
            }
        }

        group = self._es.find(search_query=search_query)
        return group[0]

    def update_delete(self):
        """
        delete document if contain field
        """

        # test
        source_string = ('ctx._source.participate.remove(ctx._source.participate.indexOf("%s"));' % "23")

        # deletes the doc if the tags field contain green
        source_string = "if (ctx._source.participate.contains(params.name)) { ctx.op = 'delete' } else { ctx.op = 'none' }"
        group = self.get_by_group_num(group_num=123)

        query = {
            'script': {
                'inline': source_string,
                'params': {
                    'name': 4
                    }}}
        try:
            server_response = self._es.update(
                index=self._index,
                doc_type=self._type,
                id=group._elastic_id,
                body=query,
                # fields=["_source", "_id", "_version"],
                # refresh=True,
                # version=group._version
                )
            result = True if server_response.get('_shards').get('successful') else False

        except RequestError as request_err:
            raise Exception(
                'invlid request to server if you work with list add empty []: %s'
                % request_err.error
            )

    def update_item(self, item, **kwargs):
        """
        updating one or more task fields
        change task status, and when status was set

        Args:
            task: Task, info about task
            status: TaskStatus, new task status
            kwargs: dict, for extra params
        Returns: True: if task updated
                 False if task NOT updated
        """

        participate = {}
        for key in kwargs:
            if key == 'participate':
                participate = kwargs.get(key)
                source_string = (
                    'ctx._source.leader=params.leader;'
                    'ctx._source.participate.%s=params.part'
                    % participate.get('name'))
                print('find source string')
                print('name: %s' % participate.get('name'))

        if source_string == '':
            return False

        query = {
            'script': {
                'inline': source_string,
                'params': {
                    'leader': participate.get('name'),
                    'part': {
                        'name': participate.get('name'),
                        'age': participate.get('age'),
                        'country': participate.get('country')
                    }}}}

        
        try:
            result = self._es.update(
                version=item.version,
                global_id=item.id,
                query=query)
        
        except Exception as ex:
            raise ex
        return result